# ER 1196

Ce dossier fournit les programmes permettant de produire les illustrations et les chiffres contenus dans le texte de l'Études et résultats n° 1196 de la DREES : "En 2020, trois Ehpad sur quatre ont eu au moins un résident infecté par la Covid-19".

Lien vers l'étude : https://drees.solidarites-sante.gouv.fr/publications/etudes-et-resultats/en-2020-trois-ehpad-sur-quatre-ont-eu-au-moins-un-resident-infecte 

Présentation de la DREES : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : enquête Covid-ESMS de Sante Publique France (dont les guides d'utilisation des deux versions sont dans le dossier "documentation"), enquête [Covid-ESMS de l'ARS-IDF](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwit9cDuqp7xAhXVPsAKHXOiBekQFjAIegQIDBAE&url=https%3A%2F%2F75.ars-iledefrance.fr%2Fgestion_codiv_ems%2Faccueil%2Ftelecharger-fichier%2F1&usg=AOvVaw1Z7_p6B6kJah-GZMJ0wadD), référentiel [Finess](https://www.data.gouv.fr/fr/datasets/finess-extraction-du-fichier-des-etablissements/), enquête [EHPA 2019](https://drees.solidarites-sante.gouv.fr/sources-outils-et-enquetes/07-lenquete-aupres-des-etablissements-dhebergement-pour-personnes-agees) (DREES), base [BADIANE](https://drees.solidarites-sante.gouv.fr/sources-outils-et-enquetes/badiane-base-interadministrative-annuelle-des-esms) (DREES). Traitements : Drees.

Les programmes ont été exécutés pour la dernière fois avec le logiciel R version 3.6.1, le 17/06/2021.


Attention : le script 6 ne sert qu'à générer les cartes au niveau régional. Il emploie le package rdrees, qui n'est pas disponible sur le CRAN. 
