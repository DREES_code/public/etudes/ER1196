# # Copyright (C) 2021. Logiciel élaboré par l’État, via la Drees.
# 
# # Nom de l’auteur : Albane Miron de l'Espinay, Drees.
# 
# # Ce programme informatique a été développé par la Drees. 
# 
# # Il permet de produire les tableaux publiés dans l’Études et Résultats 1196, intitulé « En 2020, trois Ehpad sur quatre ont eu au moins un résident infecté par la Covid-19 », juillet 2021. 
# 
# # Information sur les versions : Ce programme a été exécuté le 17/06/2021 avec la version 3.6.1 de R et les packages  margins_0.3.26    lubridate_1.7.4   forcats_0.5.0     purrr_0.3.4       readr_1.3.1       tidyr_1.0.0       tibble_3.0.3      ggplot2_3.3.3     tidyverse_1.2.1   knitr_1.25  readxl_1.3.1      httr_1.4.1        data.table_1.13.0 dplyr_1.0.6       stringr_1.4.0. 
# 
# # Le texte et les tableaux de l’article peuvent être consultés sur le site de la DREES : XXXX
# 
# # Ce programme utilise les données de l'enquête, dans leur version du 17/06/2021.
# 
# # Bien qu’il n’existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu’ils y rencontreraient, en écrivant à DREES-CODE@sante.gouv.fr
# 
# # Ce logiciel est régi par la licence “GNU General Public License” GPL-3.0. # https://spdx.org/licenses/GPL-3.0.html#licenseText
# 
# # À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
# 
# # Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence GPL-3.0, et que vous en avez accepté les termes.
# 
# # This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# # This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# # You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.


ch_data <- "<dossier d'enregistrement des données>"

## Histogrammes de l'encadré ####

dft <- read.csv2(paste0(ch_data, "episodes_nb_touches.csv"), stringsAsFactors = F)
dfd <- read.csv2(paste0(ch_data, "episodes_nb_dcd.csv"), stringsAsFactors = F)

dft$X <- NULL
dfd$X <- NULL

dft[dft == "[1,2)"] <- "Un seul"
dft[dft == "[150,800)"] <- "Plus de 150"
dft[] <- lapply(dft, function(x) gsub(")", "", x))
dft[] <- lapply(dft, function(x) gsub("\\[", "Entre ", x))
dft[] <- lapply(dft, function(x) gsub(",", " et ", x))

dft <- dft[!is.na(dft$resid_touche_groupe),]

dfd[dfd == "[0,1)"] <- "Aucun"
dfd[dfd == "[1,2)"] <- "Un seul"
dfd[dfd == "[20,150)"] <- "Plus de 20"
dfd[] <- lapply(dfd, function(x) gsub(")", "", x))
dfd[] <- lapply(dfd, function(x) gsub("\\[", "Entre ", x))
dfd[] <- lapply(dfd, function(x) gsub(",", " et ", x))

colnames(dft) <- c("Nombre de résidents touchés", "Nombre d'épisodes")
colnames(dfd) <- c("Nombre de résidents décédés", "Nombre d'épisodes")

# library(rdrees)
# 
# exporter_tableau_BPC(dft,
#                      fichier_source = paste0(ch_data, "Donnees_ER.xlsx"),
#                      titre = "Nombre d'épisodes par tranche de nombre de résidents touchés",
#                      onglet = "Graphique 1 - encadré",
#                      source = "Données Covid-ESMS (SPF), référentiel Finess, calculs DREES",
#                      champ = "Ehpad de France métropolitaine, Guadeloupe et La Réunion, première et seconde vague.",
#                      lecture = "Un seul résident a été touché par la Covid-19 pour 1926 des 9081 épisodes de Covid-19 en Ehpad déclarés en 2020.",
#                      visualiser = F,
#                      )
# 
# exporter_tableau_BPC(dfd,
#                      fichier_source = paste0(ch_data, "Donnees_ER.xlsx"),
#                      titre = "Nombre d'épisodes par tranche de nombre de résidents décédés",
#                      onglet = "Graphique 2 - encadré",
#                      source = "Données Covid-ESMS (SPF), référentiel Finess, calculs DREES",
#                      champ = "Ehpad de France métropolitaine, Guadeloupe et La Réunion, première et seconde vague.",
#                      lecture = "Un résident est décédé des suites de la Covid-19 pour 1052 des 9081 épisodes de Covid-19 en Ehpad déclarés en 2020.",
#                      visualiser = F,
# )
